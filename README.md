# Latex - Telepot Docker container

[![Docker Pulls](https://img.shields.io/docker/pulls/rig8f/latex-telepot.svg?maxAge=2592000)](https://hub.docker.com/r/rig8f/latex-telepot)
[![Docker Pulls](https://img.shields.io/docker/automated/rig8f/latex-telepot.svg?maxAge=2592000)](https://hub.docker.com/r/rig8f/latex-telepot)
[![Docker Pulls](https://img.shields.io/docker/stars/rig8f/latex-telepot.svg?maxAge=2592000)](https://hub.docker.com/r/rig8f/latex-telepot)

This container is intended to be used as a base image in CI builds of LaTeX documentation.
It is mainly designed to work with [Arara](https://github.com/cereda/arara) as build tool, instead of Latexmk.
It also contains Python's [telepot](https://github.com/nickoala/telepot) library, used to notify about builds (and their results) through a Telegram Bot.

## Features

Includes the latest available version of the following packages and utilities:

* Latest TexLive, from its repository (not the packaged version)
* Telepot library, Python 2.7 with pip
* OpenJDK 8 (only JRE)
* cURL and Wget

## Usage

Pull image ([from Hub](https://hub.docker.com/r/rig8f/latex-telepot)):

```bash
docker pull rig8f/latex-telepot
```

## Usage in CI

### Bitbucket Pipelines

Set some Environmental variables in repository's settings (_Settings > Pipelines > Environment variables_) to make the script operative, in particular:

* `TOKEN` contains the Telegram Bot Token, consider setting it as _secured_
* `FILE` contains the main TeX file, with extension (e.g. `report.tex`)
* `USERn` one variable per user that has to be notified through the bot (keep list **coherent** with the code below)
* variables starting with `BITBUCKET` are automatically set by the system

Example of `bitbucket-pipelines.yml`

```yaml
image: rig8f/latex-telepot:latest
pipelines:
  default:
    - step:
        script:
          - arara -v $FILE
          - python /data/notify.py -t $TOKEN -f $FILE -b $BITBUCKET_BRANCH -c $BITBUCKET_COMMIT -u $USER1,$USER2,$USERn
  tags:
    - step:
        script:
          - arara -v $FILE
          - python /data/notify.py -t $TOKEN -f $FILE -m $BITBUCKET_TAG -u $USER1,$USER2,$USERn
```

---

This repository is forked from [latex-docker](https://github.com/fermiumlabs/latex-docker).

(C) 2017-19 Filippo Rigotto. Released under MIT License (see LICENSE file).
