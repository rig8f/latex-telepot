#!/usr/bin/python
# (C) 2017-18 Filippo Rigotto. Released under MIT License (see LICENSE file).

# This script sends the PDF through a given Telegram Bot

from __future__ import print_function, unicode_literals
import argparse, datetime, os, sys, telepot

USAGE = 'Usage: notify.py [-q] -t bot_token -f file.pdf [-m tag] [-b branch] [-c commit_hash] -u user1,user2,...'

# read command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-q', '--quiet',  help="Suppress script output", action='store_true')
parser.add_argument('-t', '--token',  help="Telegram Bot token")
parser.add_argument('-f', '--file',   help="TeX or PDF file name")
parser.add_argument('-m', '--tag',    help="Tag in the code")
parser.add_argument('-b', '--branch', help="Branch of the code")
parser.add_argument('-c', '--commit', help="Code commit's hash")
parser.add_argument('-u', '--users',  help="Comma-separated list of Telegram user IDs")
args = parser.parse_args()

# save quiet flag
Q = args.quiet

# check for mandatory parameters
if args.token == None or args.token == '':
    if not Q:
        print('Bot token not provided, exiting.')
        print(USAGE)
    sys.exit(1)

if args.file == None or args.file == '' or not os.path.exists("./{0}".format(args.file.replace('.tex','.pdf'))):
    if not Q:
        print('PDF file not provided/found, exiting.')
        print(USAGE)
    sys.exit(1)

# compatibility mode: TeX files accepted as input
if '.tex' in args.file:
    args.file = args.file.replace('.tex','.pdf')

if args.users == None or args.users == '':
    if not Q:
        print('User list not provided, exiting.')
        print(USAGE)
    sys.exit(1)

# try connection to bot (max 5 times)
bot = None
bot_cnt = 5
while bot_cnt > 0:
    try:
        bot = telepot.Bot(args.token)
        break
    except:
        if not Q:
            print('Connection to bot failed.')
        bot_cnt -= 1

# connection failed
if bot == None:
    if not Q:
        print('Too many failed retry, exiting.')
    sys.exit(1)

if not Q:
    print('Connected to bot.')
    print('Renaming file to append date and commit.')

# build date string
now = datetime.datetime.now()
now_string = now.strftime("%Y%m%d-%H%M%S")

# file new name: append date and tag, branch and short commit hash if present
file_name = args.file.replace('.pdf','')
file_name += "_{0}".format(now_string)
if args.tag != None and args.tag != '':
    file_name += "_{0}".format(args.tag)
else:
    if args.branch != None and args.branch != '':
        file_name += "_{0}".format(args.branch)
    if args.commit != None and args.commit != '':
        file_name += "_{0}".format(args.commit[:7])
file_name += '.pdf'

# file rename
old_file_path = "./{0}".format(args.file)
new_file_path = "./{0}".format(file_name)
os.rename(old_file_path, new_file_path)
if not Q:
    print("  Old: {0}\n  New: {1}".format(args.file, file_name))

# send file to users
for id in args.users.split(','):
    with open(new_file_path, 'r') as pdf:
        if not Q:
            print("Sending PDF to {0}...".format(id))
        try:
            bot.sendDocument(id, pdf)
        except Exception as e:
            print("ERROR: {0} {1} {2}".format(id, new_file_path, e))
